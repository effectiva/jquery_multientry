# MultiEntry plugin

## Overview
Gives the ability to generate/remove desired html structure (e.g. nested forms).

> ###Requires 
>>  + html provided via ```data-prototype attribute``` 
>>  + div with ```.multientry-items``` where generated items are placed
>>  + button with ```.multientry_add``` class for adding new items

```html
  <div  class="multientry departments" 
        data-prototype="
          <div id=&quot;company_departments___name__&quot;><div>
          <input type=&quot;text&quot; id=&quot;company_departments___name___name&quot; 
          name=&quot;company[departments][__name__][name]&quot; required=&quot;required&quot; /></div>
          </div>" 
  >
    <div class="title">Department</div>
    <div class="multientry-items">
      <!-- Generated content goes here -->
    </div>
    <a href="#" class="multientry_add">Add department</a>
  </div>
```
Generated items have__ unique ID__.
>(``__name__`` is replaced with ID)

#### Generated item example:
```html
<div class="form-group multientry departments" data-prototype="
      <div id=&quot;company_departments___name__&quot;>
        <div><input type=&quot;text&quot; id=&quot;company_departments___name___name&quot; name=&quot;company[departments][__name__][name]&quot; required=&quot;required&quot; /></div>
      </div>" data-insert_first="false" data-last_item_can_be_removed="true">
    <div class="title">Department</div>

    <div class="multientry-items">

      <div id="company_departments_14301347527200" class="multientry-item new">
        <div>
          <input type="text" id="company_departments_14301347527200_name" name="company[departments][14301347527200][name]" required="required">
        </div> 
        <i class="icon-close"></i>
      </div>

    </div>

    <a href="#" class="btn multientry_add btn-primary">Add department</a>
</div>

```

## Usage:

### Via Javascript
Enable manually with:
```javascript
  $('.multientry').multientry();
```
### Options via data attributes
```html  
  <div class="multientry" data-prototype="<div id=&quot;company_departments&quot;></div>" data-insert_first="false" data-last_item_can_be_removed="true"></div>
```

### Options via Javascript
```js
  $('.multientry').multientry({
    insert_first: false,
    last_item_can_be_removed: true
  });
```

### Options

| Name                         | type            | default                                      | description                          |
| -------------                |:---------------:|:--------------------------------------------:| -------------                        |
| __insert_first__             | boolean         | true                                         | inserts first item on page load      |
| __last_item_can_be_removed__ | boolean         | false                                        | disable removing last item           |
| __limit__                    | integer         | null                                         | sets limit for adding items          |
| __show_errors__              | boolean         | true                                         | when limit is reached displays error |
| __error_message__            | string          | 'Max number of items: '+ ``this.opts.limit`` | error with max number of items       |

### Events

| Event Type                   | Description                                                     |
| -------------------          | -------------------                                             |
| __multientry:before:add   __ | This event is triggered before item is added.                   |
| __multientry:add          __ | This event is triggered immediately after item is added.        |
| __multientry:before:remove__ | This event is triggered before item is removed.                 |
| __multientry:remove       __ | This event is triggered immediately after item is removed.      |
| __multientry:limit:valid  __ | This event is triggered if number of items is within the limit. |
| __multientry:limit:reached__ | This event is triggered if number of items is ``>=`` limit      |

__Event example__
```js
  $('.multientry').on('multientry:add', function () {
  // do something…
  });
```
### Custom errors
You can __customize error message by__ passing in option ``error_message`` with desired content.

If you need __more customized errors__ then disable default errors by setting option ``show_errors`` to ``false`` and use ``limit:valid`` and ``limit:reached`` events to handle errors.

For example: 
```js
  $('.multientry').on('multientry:limit:reached', function(){
    //handle custom error    
  });
```

### DEMO
For more information try provided demo.

In order to be able to try demo you need to do the following:

  + ``git clone`` this repository
  + run ``bower install``
  + run ``http-server``

### Install instructions
__To install latest version run:__

``bower install https://git@bitbucket.org/effectiva/jquery_multientry.git#master``

__To install stable release version run:__

``bower install https://git@bitbucket.org/effectiva/jquery_multientry.git``