$(function(){
  'use strict';


  $('.multientry').multientry({
      limit: 6
    })
    .on('multientry:before:add', function(e, data){
      data.item.css({opacity:0});
    })
    .on('multientry:add', function(e, data){
      console.log(e.type, data);
      data.item.animate({opacity:1});
    })
    .on('multientry:limit:reached', function(e, data){
      alert('Limit has been reached', data.instance.opts.limit);
    });


/*  $(document.body).on('multientry:add', function(e, data){
    console.log('on', e.type, data);
     $(data.item).css({width: 0}).animate({width: '100%'},100);
  });*/

});
